package com.goit.employees.repositories;

import com.goit.employees.model.Day;
import com.goit.employees.model.DayType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DayRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DayRepository dayRepository;
    private Calendar calendar;
    private Day expectedDay;
    private Day beforeDay;
    private Day afterDay;

    @Before
    public void before() {
        calendar = Calendar.getInstance();
        calendar.set(1999, Calendar.JANUARY, 1);
        Date beforeExpected = calendar.getTime();
        calendar.set(2002, Calendar.JANUARY, 1);
        Date date = calendar.getTime();
        calendar.set(2005, Calendar.JANUARY, 1);
        Date afterExpected = calendar.getTime();

        expectedDay = new Day(date, DayType.WORKDAY);
        beforeDay = new Day(beforeExpected, DayType.WEEKEND);
        afterDay = new Day(afterExpected, DayType.HOLIDAY);
        entityManager.persist(beforeDay);
        entityManager.persist(expectedDay);
        entityManager.persist(afterDay);
        entityManager.flush();
    }

    @Test
    public void findDaysByCorrectDateBetween() {
        calendar.set(2000, Calendar.JANUARY, 1);
        Date begin = calendar.getTime();
        calendar.set(2004, Calendar.JANUARY, 1);
        Date end = calendar.getTime();
        List<Day> dayList = dayRepository.findDaysByDateBetween(begin, end);
        assertTrue(dayList.contains(expectedDay));
        assertTrue(dayList.size() == 1);
    }

    @Test
    public void findDaysByWrongDateBetween() {
        calendar.set(2003, Calendar.JANUARY, 1);
        Date begin = calendar.getTime();
        calendar.set(2004, Calendar.JANUARY, 1);
        Date end = calendar.getTime();
        List<Day> dayList = dayRepository.findDaysByDateBetween(begin, end);
        assertFalse(dayList.contains(expectedDay));
        assertTrue(dayList.size() == 0);
    }
}