package com.goit.employees.repositories;

import com.goit.employees.model.Employee;
import com.goit.employees.model.EmployeeStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Test-class of repository layer of department's entity, tests used method. This class uses alternative DB
 * for tests in-memory entities
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {

    /**
     * Manager of alternative DB
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * The repository's layer object
     */
    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * The test-method for same-named repository's default method that get one entity by ID.
     */
    @Test
    public void findOne() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.SEPTEMBER, 3);
        Date date = calendar.getTime();
        Employee employee = new Employee
                ("Ivan", "Ivanov", null, null,
                        2.0, date, EmployeeStatus.ABLE_TO_WORK);
        Employee employee1 = new Employee
                ("Semen", "Semenov", null, null,
                        1.0, date, EmployeeStatus.VACATION);
        entityManager.persist(employee);
        entityManager.persist(employee1);
        entityManager.flush();
        Employee actualEmployee = employeeRepository.findOne(employee.getId());
        assertEquals(employee, actualEmployee);
    }
}
