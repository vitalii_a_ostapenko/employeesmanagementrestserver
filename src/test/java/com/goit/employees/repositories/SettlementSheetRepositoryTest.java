package com.goit.employees.repositories;

import com.goit.employees.model.Employee;
import com.goit.employees.model.EmployeeStatus;
import com.goit.employees.model.SettlementSheet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test-class of repository layer of settlement sheet's entity, tests used methods. This class uses alternative DB
 * for tests in-memory entities
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class SettlementSheetRepositoryTest {

    /**
     * Manager of alternative DB
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * The repository's layer object
     */
    @Autowired
    private SettlementSheetRepository settlementSheetRepository;

    /**
     * The test-method for same-named repository's method, creates sheet from the pointed period. Repository's method
     * should have created sheet.
     */
    @Test
    public void findSettlementSheetsByEmployeeAndDateBetween() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.MAY, 1);
        Date date1 = calendar.getTime();
        calendar.set(2017, Calendar.AUGUST, 1);
        Date date2 = calendar.getTime();
        Employee employee = new Employee("Ivan", "Ivanov", null, null,
                2.5, date1, EmployeeStatus.ABLE_TO_WORK);
        SettlementSheet sheet = new SettlementSheet();
        sheet.setEmployee(employee);
        sheet.setMonth(Month.APRIL);
        sheet.setDate(date1);
        sheet.setPersonalIncomeTax(BigDecimal.valueOf(2124));
        sheet.setMilitaryTax(BigDecimal.valueOf(177));
        sheet.setSickLeave(BigDecimal.valueOf(0));
        sheet.setVacation(BigDecimal.valueOf(0));
        sheet.setToPayoff(BigDecimal.valueOf(9499));
        entityManager.persist(employee);
        entityManager.persist(sheet);
        entityManager.flush();

        List<SettlementSheet> actualSheets =
                settlementSheetRepository.findSettlementSheetsByEmployeeAndDateBetween(employee, date1, date2);
        assertTrue(actualSheets.contains(sheet));
    }

    /**
     * Negative test-method for same-named repository's method, creates sheet out from the pointed period.
     * Repository's method should have null.
     */
    @Test
    public void notFindSettlementSheetsByEmployeeAndDateBetween() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.MAY, 1);
        Date date1 = calendar.getTime();
        calendar.set(2017, Calendar.AUGUST, 1);
        Date date2 = calendar.getTime();
        calendar.set(2017, Calendar.DECEMBER, 1);
        Date date3 = calendar.getTime();
        Employee employee = new Employee("Ivan", "Ivanov", null, null,
                2.5, date1, EmployeeStatus.ABLE_TO_WORK);
        SettlementSheet sheet = new SettlementSheet();
        sheet.setEmployee(employee);
        sheet.setMonth(Month.APRIL);
        sheet.setDate(date1);
        sheet.setPersonalIncomeTax(BigDecimal.valueOf(2124));
        sheet.setMilitaryTax(BigDecimal.valueOf(177));
        sheet.setSickLeave(BigDecimal.valueOf(0));
        sheet.setVacation(BigDecimal.valueOf(0));
        sheet.setToPayoff(BigDecimal.valueOf(9499));
        entityManager.persist(employee);
        entityManager.persist(sheet);
        entityManager.flush();

        List<SettlementSheet> actualSheets =
                settlementSheetRepository.findSettlementSheetsByEmployeeAndDateBetween(employee, date2, date3);
        assertTrue(actualSheets.isEmpty());
    }

    /**
     * The test-method for same-named repository's default method that get one entity by ID.
     */
    @Test
    public void findOne() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.MAY, 1);
        Date date1 = calendar.getTime();
        calendar.set(2017, Calendar.SEPTEMBER, 1);
        Date date2 = calendar.getTime();
        Employee employee = new Employee("Ivan", "Ivanov", null, null,
                2.5, date1, EmployeeStatus.ABLE_TO_WORK);
        SettlementSheet sheet = new SettlementSheet();
        sheet.setEmployee(employee);
        sheet.setMonth(Month.APRIL);
        sheet.setDate(date1);
        sheet.setPersonalIncomeTax(BigDecimal.valueOf(2124));
        sheet.setMilitaryTax(BigDecimal.valueOf(177));
        sheet.setSickLeave(BigDecimal.valueOf(0));
        sheet.setVacation(BigDecimal.valueOf(0));
        sheet.setToPayoff(BigDecimal.valueOf(9499));
        SettlementSheet sheet1 = new SettlementSheet();
        sheet1.setEmployee(employee);
        sheet1.setMonth(Month.AUGUST);
        sheet1.setDate(date2);
        sheet1.setPersonalIncomeTax(BigDecimal.valueOf(2049));
        sheet1.setMilitaryTax(BigDecimal.valueOf(168));
        sheet1.setSickLeave(BigDecimal.valueOf(0));
        sheet1.setToPayoff(BigDecimal.valueOf(9021));
        sheet1.setVacation(BigDecimal.valueOf(0));
        entityManager.persist(employee);
        entityManager.persist(sheet);
        entityManager.persist(sheet1);
        entityManager.flush();

        SettlementSheet actualSheet = settlementSheetRepository.findOne(sheet.getId());
        assertEquals(sheet, actualSheet);
    }
}
