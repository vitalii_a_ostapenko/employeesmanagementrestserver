package com.goit.employees.repositories;

import com.goit.employees.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EventRepository eventRepository;
    private Employee employee1;
    private Employee employee2;
    private Event event;
    private Date eventDate;
    private Calendar calendar;

    @Before
    public void before() {
        calendar = Calendar.getInstance();
        calendar.set(2000, Calendar.JANUARY, 1);
        eventDate = calendar.getTime();
        employee1 = new Employee("Vasya", "Pupkin", null, null,
                1.0, new Date(), EmployeeStatus.ABLE_TO_WORK);
        employee2 = new Employee("Ivan", "Dulin", null, null,
                1.0, new Date(), EmployeeStatus.ABLE_TO_WORK);
        event = new Event(eventDate, 8, EventType.WORKDAY,
                Arrays.asList(EventTimeType.OVER_TIME, EventTimeType.NIGHT_TIME),
                Arrays.asList(employee1, employee2));
        entityManager.persist(employee1);
        entityManager.persist(employee2);
        entityManager.persist(event);
        entityManager.flush();
    }

    @Test
    public void findByEmployeesContainingAndCorrectDateBetween() {
        calendar.set(1999, Calendar.JANUARY, 1);
        Date beginDate = calendar.getTime();
        calendar.set(2001, Calendar.JANUARY, 1);
        Date endDate = calendar.getTime();
        List<Event> actual = eventRepository.findByEmployeesContainingAndDateBetween(employee1, beginDate, endDate);
        assertTrue(actual.contains(event));
    }

    @Test
    public void findByEmployeesContainingAndWrongDateBetween() {
        calendar.set(2001, Calendar.JANUARY, 1);
        Date beginDate = calendar.getTime();
        calendar.set(2004, Calendar.JANUARY, 1);
        Date endDate = calendar.getTime();
        List<Event> actual = eventRepository.findByEmployeesContainingAndDateBetween(employee1, beginDate, endDate);
        assertFalse(actual.contains(event));
    }
}