package com.goit.employees.repositories;

import com.goit.employees.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

/**
 * Test-class of repository layer of user's entity, tests used method. This class uses alternative DB
 * for tests in-memory entities
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    /**
     * Manager of alternative DB
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * The repository's layer object
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * The test-method for same-named repository's default method that get one entity by ID.
     */
    @Test
    public void findOne() {
        User user = new User("email1", "pass", null, null);
        User user2 = new User("email2", "pass2", null, null);
        entityManager.persist(user);
        entityManager.persist(user2);
        entityManager.flush();

        User actualUser = userRepository.findOne(user.getId());

        assertEquals(user, actualUser);
    }
}

