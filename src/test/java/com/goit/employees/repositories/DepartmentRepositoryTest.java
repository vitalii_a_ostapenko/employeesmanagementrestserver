package com.goit.employees.repositories;

import com.goit.employees.model.Department;
import com.goit.employees.model.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test-class of repository layer of department's entity, tests used method. This class uses alternative DB
 * for tests in-memory entities
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DepartmentRepositoryTest {

    /**
     * Manager of alternative DB
     */
    @Autowired
    private TestEntityManager entityManager;
    /**
     * The repository's layer object
     */
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * The test-method for same-named repository's default method that get one entity by ID.
     */
    @Test
    public void findOne() {
        Position position = new Position("Position1", BigDecimal.valueOf(1), null, null);
        List<Position> positions = new ArrayList<>();
        positions.add(position);
        Department department = new Department("Department1", positions);
        Department department1 = new Department("Department2", positions);
        entityManager.persist(department);
        entityManager.persist(department1);
        entityManager.flush();
        Department expectedDepartment = departmentRepository.findOne(department.getId());
        assertEquals(expectedDepartment, department);
    }
}
