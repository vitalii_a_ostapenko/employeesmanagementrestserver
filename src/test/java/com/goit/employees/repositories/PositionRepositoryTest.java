package com.goit.employees.repositories;

import com.goit.employees.model.Position;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Test-class of repository layer of position's entity, tests used method. This class uses alternative DB
 * for tests in-memory entities
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PositionRepositoryTest {

    /**
     * Manager of alternative DB
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * The repository's layer object
     */
    @Autowired
    private PositionRepository positionRepository;

    /**
     * The test-method for same-named repository's default method that get one entity by ID.
     */
    @Test
    public void findOne() {
        Position position = new Position("Position1", BigDecimal.valueOf(100), null, null);
        Position position1 = new Position("Position2", BigDecimal.valueOf(120), null, null);
        entityManager.persist(position);
        entityManager.persist(position1);
        entityManager.flush();

        Position actualPosition = positionRepository.findOne(position.getId());
        assertEquals(position, actualPosition);
    }
}
