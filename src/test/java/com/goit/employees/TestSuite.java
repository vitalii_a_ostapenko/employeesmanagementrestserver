package com.goit.employees;

import com.goit.employees.services.CountServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({EmployeesApplicationTests.class, CountServiceTest.class})
public class TestSuite {

}
