package com.goit.employees;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({DbSuite.class, TestSuite.class})
public class AllSuite {
}
