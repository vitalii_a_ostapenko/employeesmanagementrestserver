package com.goit.employees.services;

import com.goit.employees.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Test-class for test calculation of service-class {@link com.goit.employees.services.CountService},
 * contains an test-models and test-methods
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CountServiceTest {

    @Autowired
    private CountService countService;
    private Department department = new Department();
    private Position position = new Position();
    private Employee employee = new Employee();
    private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
    private List<Event> events = new ArrayList<>();
    private List<Employee> employees = new ArrayList<>();
    private List<Day> days = new ArrayList<>();
    private List<EventTimeType> eventTimeTypes;
    private Calendar calendar = Calendar.getInstance();
    private SettlementSheet settlementSheet = new SettlementSheet();
    private List<SettlementSheet> sheets = new ArrayList<>();
    private Date monthAgo = null;

    /**
     * The method creates models with testing parameters. Calendar days are created for november and
     * december. On day's basis the events are created for november and december. In december
     * is considered that employee has a sick leave from 20/12/2017 to the end of month.
     * Testing settlement sheet for november with parameters
     */
    @Before
    public void initializationModels() {

        //____________Department________________________
        department.setName("Department1");
        //_____________Position________________________
        position.setDepartment(department);
        position.setHourlyRate(BigDecimal.valueOf(100.00));
        position.setName("Developer1");

        //_______________Employee________________________
        employee.setFirstName("Ivan");
        employee.setLastName("Ivanov");
        Date employmentDate = null;
        try {
            employmentDate = format.parse("01.11.2017");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        employee.setEmploymentDate(employmentDate);
        employee.setPosition(position);
        employee.setStartingWorkExperience(3.5);
        employee.setStatus(EmployeeStatus.ABLE_TO_WORK);
        //_________Day______________________________
        //November
        try {
            calendar.setTime(format.parse("01.11.2017"));

            for (int i = 1; i <= 30; i++) {
                Day day = new Day();
                day.setDate(calendar.getTime());
                day.setDayType(DayType.WORKDAY);
                for (int j = 4; j <= 30; j += 7) {
                    if (i == j || i == j + 1) {
                        day.setDayType(DayType.WEEKEND);
                    }
                }
                days.add(day);
                calendar.add(Calendar.DATE, 1);
            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        //December
        try {
            calendar.setTime(format.parse("01.12.2017"));

            for (int i = 1; i <= 31; i++) {
                Day day = new Day();
                day.setDate(calendar.getTime());
                day.setDayType(DayType.WORKDAY);
                for (int j = 2; j <= 31; j += 7) {
                    if (i == j || i == j + 1) {
                        day.setDayType(DayType.WEEKEND);
                    }
                }
                if (i == 25) {
                    day.setDayType(DayType.HOLIDAY);
                }
                days.add(day);
                calendar.add(Calendar.DATE, 1);
            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        //_______Event____________________________
        employees.add(employee);
        Date illnessDay;
        try {
            illnessDay = format.parse("21.12.2017");
            for (Day day : days) {
                Event event = new Event();
                event.setDate(day.getDate());
                event.setEmployees(employees);
                if (day.getDate().getTime() < illnessDay.getTime()) {
                    if (day.getDayType().equals(DayType.HOLIDAY)) {
                        eventTimeTypes = new ArrayList<>();
                        eventTimeTypes.add(EventTimeType.HOLIDAY);
                        event.setEventTimeTypes(eventTimeTypes);
                        event.setEventType(EventType.WORKDAY);
                        event.setHours(4);                      // 0 day*4*200 = 0.00 uah
                    }
                    if (day.getDayType().equals(DayType.WORKDAY)) {
                        event.setEventType(EventType.WORKDAY);
                        eventTimeTypes = new ArrayList<>();
                        eventTimeTypes.add(EventTimeType.WORKDAY);
                        event.setEventTimeTypes(eventTimeTypes);
                        event.setHours(8);                 // 14 days*8*100 = 11 200.00 uah - december
                    }
                } else {
                    event.setEventType(EventType.HOSPITAL);   // 11 day*1600/30*coef 0.6 = 352.00 uah
                }
                events.add(event);                              // total salary 11 552.00, incTax = 2079.36, milit = 173.28
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //_____________Settlement sheet_______________
        Month month = Month.valueOf("NOVEMBER");
        settlementSheet.setToPayoff(BigDecimal.valueOf(1288.00));
        settlementSheet.setVacation(BigDecimal.valueOf(0.00));
        settlementSheet.setSickLeave(BigDecimal.valueOf(0.00));
        settlementSheet.setMonth(month);
        settlementSheet.setPersonalIncomeTax(BigDecimal.valueOf(288.00));
        settlementSheet.setMilitaryTax(BigDecimal.valueOf(24));
        try {
            settlementSheet.setDate(format.parse("01.12.2017"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        settlementSheet.setEmployee(employee);

        sheets.add(settlementSheet);
        //____________Date________________________________
        try {
            monthAgo = format.parse("01.12.2017");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test to the method that counts compensation of sick leave
     * Expected value was found as: number of illness days (11) * total salary for previous period from test previous
     * sheet (1600) / number of calendar days previous period (30) * coefficient  sick.coefficient.to-five-year=0.6
     * (0.6) = 352.00
     */
    @Test
    public void countSickLeaveTest() {
        BigDecimal value = countService.countSickLeave(employee, sheets, events, monthAgo);
        BigDecimal expectValue = BigDecimal.valueOf(352.00);
        Assert.assertEquals(expectValue.doubleValue(), value.doubleValue(), 0.05);
    }

    /**
     * Test to method that counts a vacancy payment. Number of vacancy days = 0
     */
    @Test
    public void countVacancyTest() {
        BigDecimal value = countService.countVacancy(sheets, events, days, monthAgo);
        BigDecimal expectValue = BigDecimal.valueOf(0.00);
        Assert.assertEquals(expectValue.doubleValue(), value.doubleValue(), 0.05);
    }

    /**
     * Test to method that counts a military tax.
     * Expected value was found as: Salary for december(14 working days * 8 hour per day *
     * 100 hourly rate) (11200.00) + compensation of sick leave (352.00) = Total salary 11 552.00.
     * Total salary (11552.00) * military tax rate (0.015) = 173.28
     */
    @Test
    public void militaryTest() {
        BigDecimal value = countService.militaryTax(events, employee, sheets, days, monthAgo);
        BigDecimal expectValue = BigDecimal.valueOf(173.28);
        Assert.assertEquals(expectValue.doubleValue(), value.doubleValue(), 0.05);
    }

    /**
     * Test to method that counts income personal tax.
     * Expected value was found as: Salary for december(14 working days * 8 hour per day *
     * 100 hourly rate) (11200.00) + compensation of sick leave (352.00) = Total salary 11 552.00.
     * Total salary (11552.00) * personal income tax rate (0.18) = 2079.36
     */
    @Test
    public void personalIncomeTaxTest() {
        BigDecimal value = countService.personalIncomeTax(events, employee, sheets, days, monthAgo);
        BigDecimal expectValue = BigDecimal.valueOf(2079.36);
        Assert.assertEquals(expectValue.doubleValue(), value.doubleValue(), 0.05);
    }

    /**
     * Test to method that counts salary payment.
     * Expected value was found as: Salary for december(14 working days * 8 hour per day *
     * 100 hourly rate) (11200.00) + compensation of sick leave (352.00) = Total salary 11 552.00.
     * Total salary (11552.00) - military tax (173.28) - personal income tax (2079.36) = 9299.36
     */
    @Test
    public void salaryToPayTest() {
        BigDecimal value = countService.salaryToPay(events, employee, sheets, days, monthAgo);
        BigDecimal expectValue = BigDecimal.valueOf(9299.36);
        Assert.assertEquals(expectValue.doubleValue(), value.doubleValue(), 0.05);
    }
}
