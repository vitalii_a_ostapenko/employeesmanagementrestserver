package com.goit.employees.mapper;

import com.goit.employees.TestConfiguration;
import com.goit.employees.dto.DayCreateDTO;
import com.goit.employees.dto.EventCreateDTO;
import com.goit.employees.dto.PositionCreateDTO;
import com.goit.employees.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(TestConfiguration.class)
public class PersistenceDtoMapperTest {
    @Autowired
    private PersistenceDtoMapper mapper;

    @Autowired
    private TestEntityManager entityManager;
    private Department department;

    @Before
    public void before() {

    }

    @Test
    public void mapSimpleDTO() {
        Date date = new Date();
        DayType dayType = DayType.WORKDAY;
        DayCreateDTO dto = new DayCreateDTO(date, dayType);
        Day day = mapper.map(dto, Day.class);
        assertEquals(date, day.getDate());
        assertEquals(dayType, day.getDayType());
        assertNull(day.getId());
    }

    @Test
    public void mapWithDependentEntity() {
        department = new Department();
        department.setName("department");
        entityManager.persist(department);
        entityManager.flush();
        PositionCreateDTO dto = new PositionCreateDTO("worker", BigDecimal.valueOf(10), department.getId());
        Position position = mapper.map(dto, Position.class);
        assertEquals(department, position.getDepartment());
    }

    @Test
    public void mapWithDependentEntitiesList() {
        Employee employee1 = new Employee("Vasya", "Pupkin", null, null,
                1.0, new Date(), EmployeeStatus.ABLE_TO_WORK);
        Employee employee2 = new Employee("Ivan", "Dulin", null, null,
                1.0, new Date(), EmployeeStatus.ABLE_TO_WORK);
        entityManager.persist(employee1);
        entityManager.persist(employee2);
        entityManager.flush();
        Long[] ids = {employee1.getId(), employee2.getId()};
        EventTimeType[] timeTypes = {EventTimeType.WORKDAY, EventTimeType.HOLIDAY};
        EventCreateDTO dto = new EventCreateDTO(new Date(), 8,EventType.WORKDAY, ids, timeTypes);
        Event event = mapper.map(dto, Event.class);
        assertTrue(event.getEmployees().size() == 2);
        assertTrue(event.getEmployees().contains(employee1));
        assertTrue(event.getEmployees().contains(employee2));
        assertTrue(event.getHours() == 8);
        assertEquals(event.getDate(), dto.getDate());
    }
}