package com.goit.employees;

import com.goit.employees.mapper.PersistenceDtoMapper;
import org.springframework.context.annotation.Bean;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
    @Bean
    public PersistenceDtoMapper mapper() {
        return new PersistenceDtoMapper();
    }
}
