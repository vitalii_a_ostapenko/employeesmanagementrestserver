package com.goit.employees;

import com.goit.employees.mapper.PersistenceDtoMapperTest;
import com.goit.employees.repositories.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DayRepositoryTest.class, DepartmentRepositoryTest.class, EmployeeRepositoryTest.class,
        EventRepositoryTest.class, PositionRepositoryTest.class, SettlementSheetRepositoryTest.class,
        UserRepositoryTest.class, PersistenceDtoMapperTest.class
})
public class DbSuite {
}
