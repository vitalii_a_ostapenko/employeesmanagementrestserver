package com.goit.employees.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * The {@link DepartmentUpdateDTO} to update a {@link com.goit.employees.model.Department} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class DepartmentUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;
    @ApiModelProperty(required = true, position = 2)
    @NotNull(message = "name must be not null")
    private String name;
}
