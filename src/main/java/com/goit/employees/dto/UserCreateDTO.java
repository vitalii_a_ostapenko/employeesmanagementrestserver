package com.goit.employees.dto;

import com.goit.employees.mapper.Entity;
import com.goit.employees.model.Employee;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The {@link UserCreateDTO} to create a {@link com.goit.employees.model.User} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class UserCreateDTO {
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "email must be not null")
    private String email;

    @ApiModelProperty(required = true, position = 2)
    @NotNull(message = "confirmEmail must be not null")
    private String confirmEmail;

    @ApiModelProperty(required = true, position = 3)
    @NotNull(message = "password must be not null")
    @Size(min = 8, max = 25, message = "password length must be 8-25 symbols")
    private String password;

    @ApiModelProperty(required = true, position = 4)
    @NotNull(message = "confirmPassword must be not null")
    @Size(min = 8, max = 25, message = "confirmPassword length must be 8-25 symbols")
    private String confirmPassword;

    @Entity(Employee.class)
    @ApiModelProperty(required = true, position = 5)
    @NotNull(message = "employeeId must be not null")
    private Long employeeId;

    @AssertTrue(message = "emails does not matches")
    private boolean isEmailsMatches() {
        return email.equals(confirmEmail);
    }

    @AssertTrue(message = "passwords does not matches")
    private boolean isPasswordsMatches() {
        return password.equals(confirmPassword);
    }
}
