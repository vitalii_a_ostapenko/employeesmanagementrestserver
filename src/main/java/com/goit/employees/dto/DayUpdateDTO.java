package com.goit.employees.dto;

import com.goit.employees.model.DayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * The {@link DayUpdateDTO} to update a {@link com.goit.employees.model.Day} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class DayUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;

    @ApiModelProperty(position = 2)
    private Date date;

    @ApiModelProperty(position = 3)
    private DayType dayType;
}
