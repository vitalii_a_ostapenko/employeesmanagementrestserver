package com.goit.employees.dto;

import com.goit.employees.model.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * The {@link UserUpdateDTO} to update a {@link com.goit.employees.model.User} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class UserUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;

    @ApiModelProperty(position = 2)
    private String email;

    @ApiModelProperty(position = 3)
    private Role role;
}
