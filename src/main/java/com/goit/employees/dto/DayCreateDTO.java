package com.goit.employees.dto;

import com.goit.employees.model.DayType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * The {@link DayCreateDTO} to create a {@link com.goit.employees.model.Day} entity by Rest Controller.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class DayCreateDTO {
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "date must be not null")
    private Date date;

    @ApiModelProperty(required = true, position = 2)
    @NotNull(message = "dayType must be not null")
    private DayType dayType;
}
