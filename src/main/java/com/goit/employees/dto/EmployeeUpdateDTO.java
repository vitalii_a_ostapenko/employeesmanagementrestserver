package com.goit.employees.dto;

import com.goit.employees.mapper.Entity;
import com.goit.employees.model.EmployeeStatus;
import com.goit.employees.model.Position;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * The {@link EmployeeUpdateDTO} to update a {@link com.goit.employees.model.Employee} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class EmployeeUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;

    @ApiModelProperty(position = 2)
    private String firstName;

    @ApiModelProperty(position = 3)
    private String lastName;

    @ApiModelProperty(position = 4)
    private EmployeeStatus status;

    @Entity(Position.class)
    @ApiModelProperty(position = 5)
    private Long positionId;
}
