package com.goit.employees.dto;

import com.goit.employees.mapper.Entity;
import com.goit.employees.model.Employee;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * The {@link PositionUpdateDTO} to update a {@link com.goit.employees.model.Position} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class PositionUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;

    @ApiModelProperty(position = 2)
    private String name;

    @ApiModelProperty(position = 3)
    private BigDecimal hourlyRate;

    @ApiModelProperty(position = 4)
    private Long departmentId;

    @ApiModelProperty(position = 5)
    @Entity(Employee.class)
    private Long employeeId;
}
