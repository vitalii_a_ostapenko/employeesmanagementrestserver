package com.goit.employees.dto;

import com.goit.employees.mapper.Entity;
import com.goit.employees.model.Department;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * The {@link PositionCreateDTO} to create a {@link com.goit.employees.model.Position} entity by Rest Controller.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class PositionCreateDTO {

    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "name must be not null")
    private String name;

    @ApiModelProperty(required = true, position = 2)
    @Min(value = 0, message = "hourly rest must be 0 and greater")
    @NotNull(message = "hourlyRate must be not null")
    private BigDecimal hourlyRate;

    @Entity(Department.class)
    @ApiModelProperty(required = true, position = 3)
    @NotNull(message = "departmentId must be not null")
    private Long departmentId;
}
