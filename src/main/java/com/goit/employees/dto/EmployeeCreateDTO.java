package com.goit.employees.dto;

import com.goit.employees.mapper.Entity;
import com.goit.employees.model.Position;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * The {@link EmployeeCreateDTO} to create a {@link com.goit.employees.model.Employee} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class EmployeeCreateDTO {
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "firstName must be not null")
    private String firstName;

    @ApiModelProperty(required = true, position = 2)
    @NotNull(message = "lastName must be not null")
    private String lastName;

    @Entity(Position.class)
    @ApiModelProperty(required = true, position = 3)
    @NotNull(message = "positionId must be not null")
    private Long positionId;

    @ApiModelProperty(required = true, position = 4)
    @Min(value = 0, message = "startingWorkExperience must be 0 and greater")
    @NotNull(message = "startingWorkExperience must be not null")
    private Double startingWorkExperience;

    @ApiModelProperty(required = true, position = 5)
    @NotNull(message = "employmentDate must be not null")
    private Date employmentDate;
}
