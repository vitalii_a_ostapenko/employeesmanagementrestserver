package com.goit.employees.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * The {@link DepartmentCreateDTO} to create a {@link com.goit.employees.model.Department} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class DepartmentCreateDTO {
    @ApiModelProperty(required = true)
    @NotNull(message = "name must be not null")
    private String name;
}
