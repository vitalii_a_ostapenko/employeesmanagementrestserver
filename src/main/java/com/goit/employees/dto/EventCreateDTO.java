package com.goit.employees.dto;

import com.goit.employees.mapper.EntityList;
import com.goit.employees.model.Employee;
import com.goit.employees.model.EventTimeType;
import com.goit.employees.model.EventType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * The {@link EventCreateDTO} to create a {@link com.goit.employees.model.Event} entity by Rest Controller.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class EventCreateDTO {
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "date must be not null")
    private Date date;

    @ApiModelProperty(required = true, position = 2)
    @Min(value = 1, message = "hours must be 1 and greater")
    private int hours;

    @ApiModelProperty(required = true, position = 3)
    @NotNull(message = "eventType must be not null")
    private EventType eventType;

    @EntityList(value = Employee.class, fieldName = "employees")
    @ApiModelProperty(required = true, position = 4)
    @NotNull(message = "employeesId must be not null")
    private Long[] employeesId;

    @ApiModelProperty(required = true, position = 5)
    @NotNull(message = "eventTimeTypes must be not null")
    private EventTimeType[] eventTimeTypes;
}
