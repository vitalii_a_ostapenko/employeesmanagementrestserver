package com.goit.employees.dto;

import com.goit.employees.mapper.EntityList;
import com.goit.employees.model.Employee;
import com.goit.employees.model.EventTimeType;
import com.goit.employees.model.EventType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * The {@link EventUpdateDTO} to update a {@link com.goit.employees.model.Event} entity by Rest Controller.
 */
@Getter
@Setter
@ApiModel
public class EventUpdateDTO {
    @Id
    @ApiModelProperty(required = true, position = 1)
    @NotNull(message = "id must be not null")
    private Long id;

    @ApiModelProperty(position = 2)
    private Date date;

    @ApiModelProperty(position = 3)
    @Min(value = 1, message = "hours must be 1 and greater")
    private int hours;

    @ApiModelProperty(position = 4)
    private EventType eventType;

    @ApiModelProperty(position = 5)
    @EntityList(value = Employee.class, fieldName = "employees")
    private Long[] employeesId;

    @ApiModelProperty(position = 6)
    private EventTimeType[] eventTimeTypes;
}
