package com.goit.employees.model;

/**
 * The types of some event type.
 */
public enum EventType {
    /**
     * The workday, hospital, technical training and vacation event type.
     */
    WORKDAY, HOSPITAL, TECHNICAL_TRAINING, VACATION, ABSENT
}
