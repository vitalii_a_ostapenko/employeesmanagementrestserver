package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goit.employees.json.PositionSerializer;
import lombok.*;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The class implements a set of methods for working
 * with entities of the {@link Position} class.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"department", "employee"})
@Entity
@Table(name = "positions")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Position.class)
@JsonSerialize(using = PositionSerializer.class)
public class Position extends BaseEntity {

    /**
     * The name of this position.
     */
    @Column(name = "name")
    private String name;

    /**
     * The hourly rate of this position.
     */
    @Column(name = "hourly_rate")
    private BigDecimal hourlyRate;

    /**
     * The department of this position.
     */
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    /**
     * The employee of this position.
     */
    @Nullable
    @OneToOne(mappedBy = "position")
    private Employee employee;
}
