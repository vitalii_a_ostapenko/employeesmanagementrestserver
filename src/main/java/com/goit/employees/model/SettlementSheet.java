package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Month;
import java.util.Date;

/**
 * The class implements a set of methods for working
 * with entities of the {@link SettlementSheet} class.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "employee")
@Entity
@Table(name = "settlement_sheets")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = SettlementSheet.class)
public class SettlementSheet extends BaseEntity {

    /**
     * The settlement sheet of this employee.
     */
    @ManyToOne
    @JoinColumn(name = "employee_id")
    @ApiModelProperty(dataType = "com.goit.employees.swagger.dummy.employee.EmployeeDummy", position = 1)
    private Employee employee;

    /**
     * This field shows the month of the settlement sheet.
     */
    @Column(name = "month")
    @Enumerated(EnumType.ORDINAL)
    @ApiModelProperty(position = 2)
    private Month month;

    /**
     * The date of this settlement sheet.
     */
    @Column(name = "date")
    @ApiModelProperty(position = 3)
    private Date date;

    /**
     * The personal income tax of this settlement sheet.
     */
    @Column(name = "personal_income_tax")
    @ApiModelProperty(position = 4)
    private BigDecimal personalIncomeTax;

    /**
     * The military tax of this settlement sheet.
     */
    @Column(name = "military_tax")
    @ApiModelProperty(position = 5)
    private BigDecimal militaryTax;

    /**
     * The vacation of this settlement sheet.
     */
    @Column(name = "vacation")
    @ApiModelProperty(position = 6)
    private BigDecimal vacation;

    /**
     * The sick leave of this settlement sheet.
     */
    @Column(name = "sick_leave")
    @ApiModelProperty(position = 7)
    private BigDecimal sickLeave;

    /**
     * The to payoff of this settlement sheet.
     */
    @Column(name = "to_pay_off")
    @ApiModelProperty(position = 8)
    private BigDecimal toPayoff;
}
