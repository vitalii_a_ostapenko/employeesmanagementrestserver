package com.goit.employees.model;

/**
 * The types of employee status.
 */
public enum EmployeeStatus {

    /**
     * The hospital, vacation, able to work employee status.
     */
    HOSPITAL, VACATION, ABLE_TO_WORK
}
