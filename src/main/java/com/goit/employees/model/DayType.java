package com.goit.employees.model;

/**
 * The types of some day.
 */
public enum DayType {

    /**
     * The workday, holiday, weekend day type.
     */
    WORKDAY, HOLIDAY, WEEKEND
}
