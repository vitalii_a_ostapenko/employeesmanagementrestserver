package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goit.employees.json.DepartmentSerializer;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * The class implements a set of methods for working
 * with entities of the {@link Department} class.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode (callSuper = true, exclude = "positions")
@Table(name = "departments")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Department.class)
@JsonSerialize(using = DepartmentSerializer.class)
public class Department extends BaseEntity {

    /**
     * The name of this department.
     */
    @Column(name = "name")
    private String name;

    /**
     * The list positions of this department.
     */
    @OneToMany(mappedBy = "department")
    private List<Position> positions;
}
