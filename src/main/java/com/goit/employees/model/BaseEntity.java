package com.goit.employees.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * The class implements a set of methods for working
 * with entities of the {@link BaseEntity} class.
 */
@MappedSuperclass
@EqualsAndHashCode
@Getter
@Setter
public class BaseEntity {

    /**
     * The unique identifier for each entity.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    protected Long id;
}
