package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The class implements a set of methods for working
 * with entities of the {@link Event} class.
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"employees"})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "events")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Event.class)
public class Event extends BaseEntity {

    /**
     * The date of this event.
     */
    @Column(name = "date")
    @ApiModelProperty(position = 1)
    private Date date;

    /**
     * The hours of this event.
     */
    @Column(name = "hours")
    @ApiModelProperty(position = 2)
    private int hours;

    /**
     * The event type of this event.
     */
    @Column(name = "event_type")
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(position = 3)
    private EventType eventType;

    /**
     * The list event time types of this event.
     */
    @ElementCollection(targetClass = EventTimeType.class)
    @JoinTable(name = "events_event_time_types", joinColumns = @JoinColumn(name = "event_id"))
    @Column(name = "event_time_type", nullable = false)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(position = 4)
    private List<EventTimeType> eventTimeTypes;

    /**
     * The list employees of this event.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "employees_events", joinColumns = @JoinColumn(name = "event_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id"))
    @ApiModelProperty(dataType = "com.goit.employees.swagger.dummy.employee.EmployeeDummyList", position = 5)
    private List<Employee> employees;


}
