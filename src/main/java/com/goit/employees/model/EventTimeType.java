package com.goit.employees.model;

/**
 * The types of some event time type.
 */
public enum EventTimeType {
    /**
     * The night time, over time, holiday and weekend event time type.
     */
    NIGHT_TIME, OVER_TIME, HOLIDAY, WEEKEND, WORKDAY
}
