package com.goit.employees.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * The class implements a set of methods for working
 * with entities of the {@link Day} class.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "days")
public class Day extends BaseEntity {

    /**
     * The date of this day.
     */
    @Column(name = "date")
    @ApiModelProperty(position = 1)
    private Date date;

    /**
     * The day type of this day.
     */
    @Column(name = "day_type")
    @ApiModelProperty(position = 2)
    @Enumerated(EnumType.STRING)
    private DayType dayType;

}
