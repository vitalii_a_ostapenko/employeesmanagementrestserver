package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goit.employees.json.EmployeeSerializer;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * The class implements a set of methods for working
 * with entities of the {@link Employee} class.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"user", "position"})
@Entity
@Table(name = "employees")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Employee.class)
@JsonSerialize(using = EmployeeSerializer.class)
public class Employee extends BaseEntity {

    /**
     * The first name of this employee.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * The last name of this employee.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * The user of this employee.
     */
    @OneToOne(mappedBy = "employee")
    private User user;

    /**
     * The position of this employee.
     */
    @OneToOne
    @JoinColumn(name = "position_id")
    private Position position;

    /**
     * The date starting work experience of this employee.
     */
    @Column(name = "starting_work_experience")
    private Double startingWorkExperience;

    /**
     * The employment date of this employee.
     */
    @Column(name = "employment_date")
    private Date employmentDate;

    /**
     * The status of this employee.
     */
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EmployeeStatus status;
}
