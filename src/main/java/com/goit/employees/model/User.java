package com.goit.employees.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;

/**
 * The class implements a set of methods for working
 * with entities of the {@link User} class.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = "employee")
@Entity
@Table(name = "users")
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = User.class)
public class User extends BaseEntity {

    /**
     * The email of this user.
     */
    @Column(name = "email")
    private String email;

    /**
     * The password of this user.
     */
    @Column(name = "password")
    private String password;

    /**
     * The role of this user.
     */
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * user specified for this employee.
     */
    @OneToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
}
