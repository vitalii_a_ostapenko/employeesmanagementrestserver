package com.goit.employees.model;

/**
 * The types of some role.
 */
public enum Role {

    /**
     * The admin, moderator, user role.
     */
    ADMIN, MODERATOR, USER
}
