package com.goit.employees.rest;

import com.goit.employees.dto.EmployeeCreateDTO;
import com.goit.employees.dto.EmployeeUpdateDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Employee;
import com.goit.employees.model.EmployeeStatus;
import com.goit.employees.services.impl.EmployeeServiceImpl;
import com.goit.employees.swagger.dummy.employee.EmployeeDummy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "employeeservice", description = "Operations with employees")
public class EmployeeController {

    @Autowired
    private PersistenceDtoMapper mapper;
    @Autowired
    private EmployeeServiceImpl employeeService;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PostMapping("/employees")
    @ApiOperation(value = "Add an employee")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addEmployee(@Valid @RequestBody EmployeeCreateDTO dto) {
        Employee employee = mapper.map(dto, Employee.class);
        employee.setStatus(EmployeeStatus.ABLE_TO_WORK);
        employeeService.add(employee);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PutMapping("/employees")
    @ApiOperation(value = "Update an employee")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateEmployee(@Valid @RequestBody EmployeeUpdateDTO dto) {
        Employee employee = mapper.map(dto, Employee.class);
        employeeService.update(employee);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @DeleteMapping("/employees/{id}")
    @ApiOperation(value = "Delete an employee")
    public void deleteEmployeeById(@PathVariable Long id) {
        employeeService.deleteById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/employees/{id}")
    @ApiOperation(value = "Search an employee by ID", response = EmployeeDummy.class)
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeService.getById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/employees")
    @ApiOperation(value = "View a list of available employees", response = EmployeeDummy.class, responseContainer = "List")
    public List<Employee> getAllEmployees() {
        return employeeService.getAll();
    }
}
