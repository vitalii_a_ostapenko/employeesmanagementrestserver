package com.goit.employees.rest;

import com.goit.employees.dto.PositionCreateDTO;
import com.goit.employees.dto.PositionUpdateDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Position;
import com.goit.employees.services.impl.PositionServiceImpl;
import com.goit.employees.swagger.dummy.position.PositionDummy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
@RestController
@Api(value = "positionservice", description = "Operations with positions")
public class PositionController {

    @Autowired
    private PersistenceDtoMapper mapper;
    @Autowired
    private PositionServiceImpl positionService;

    @PostMapping("/positions")
    @ApiOperation(value = "Add a position")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addPosition(@Valid @RequestBody PositionCreateDTO dto) {
        Position position = mapper.map(dto, Position.class);
        positionService.add(position);
    }

    @PutMapping("/positions")
    @ApiOperation(value = "Update a position")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updatePosition(@Valid @RequestBody PositionUpdateDTO dto) {
        Position position = mapper.map(dto, Position.class);
        positionService.update(position);
    }

    @DeleteMapping("/positions/{id}")
    @ApiOperation(value = "Delete a position")
    public void deletePositionById(@PathVariable Long id) {
        positionService.deleteById(id);
    }

    @GetMapping("/positions/{id}")
    @ApiOperation(value = "Search a position by ID", response = PositionDummy.class)
    public Position getPositionById(@PathVariable Long id) {
        return positionService.getById(id);
    }

    @GetMapping("/positions")
    @ApiOperation(value = "View a list of available positions", response = PositionDummy.class, responseContainer = "List")
    public List<Position> getAllPositions() {
        return positionService.getAll();
    }
}
