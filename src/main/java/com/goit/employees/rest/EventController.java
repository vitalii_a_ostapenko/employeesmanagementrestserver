package com.goit.employees.rest;

import com.goit.employees.dto.EventCreateDTO;
import com.goit.employees.dto.EventUpdateDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Event;
import com.goit.employees.services.impl.EventServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "eventservice", description = "Operations with events")
public class EventController {

    @Autowired
    private PersistenceDtoMapper mapper;
    @Autowired
    private EventServiceImpl eventService;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PostMapping("/events")
    @ApiOperation(value = "Add an event")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addEvent(@Valid @RequestBody EventCreateDTO dto) {
        Event event = mapper.map(dto, Event.class);
        eventService.add(event);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PutMapping("/events")
    @ApiOperation(value = "Update an event")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateEvent(@Valid @RequestBody EventUpdateDTO dto) {
        Event event = mapper.map(dto, Event.class);
        eventService.update(event);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @DeleteMapping("/events/{id}")
    @ApiOperation(value = "Delete an event")
    public void deleteEventById(@PathVariable Long id) {
        eventService.deleteById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/events/{id}")
    @ApiOperation(value = "Search an event by ID", response = Event.class)
    public Event getEventById(@PathVariable Long id) {
        return eventService.getById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/events")
    @ApiOperation(value = "View a list of available events", response = Event.class, responseContainer = "List")
    public List<Event> getAllEvents() {
        return eventService.getAll();
    }
}
