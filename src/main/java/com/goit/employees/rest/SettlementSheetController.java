package com.goit.employees.rest;

import com.goit.employees.model.SettlementSheet;
import com.goit.employees.services.impl.SettlementSheetServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "settlementservice", description = "Operations with settlement sheets")
public class SettlementSheetController {

    @Autowired
    private SettlementSheetServiceImpl settlementSheetService;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR","ROLE_USER"})
    @GetMapping("/settlements/{id}")
    @ApiOperation(value = "Search a settlement sheet by ID", response = SettlementSheet.class)
    public SettlementSheet getSettlementSheetById(@PathVariable Long id) {
        return settlementSheetService.getById(id);
    }
}
