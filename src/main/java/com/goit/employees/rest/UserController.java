package com.goit.employees.rest;

import com.goit.employees.dto.UserCreateDTO;
import com.goit.employees.dto.UserReadDTO;
import com.goit.employees.dto.UserUpdateDTO;
import com.goit.employees.dto.UserUpdatePasswordDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Role;
import com.goit.employees.model.User;
import com.goit.employees.services.impl.UserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "userservice", description = "Operations with users")
public class UserController {

    @Autowired
    private PersistenceDtoMapper mapper;
    @Autowired
    private UserServiceImpl userService;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PostMapping("/users")
    @ApiOperation(value = "Add a user")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addUser(@Valid @RequestBody UserCreateDTO dto) {
        User user = mapper.map(dto, User.class);
        user.setRole(Role.USER);
        userService.add(user);
    }

    @Secured({"ROLE_ADMIN"})
    @PutMapping("/users")
    @ApiOperation(value = "Update a user")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateUser(@Valid @RequestBody UserUpdateDTO dto) {
        User user = mapper.map(dto, User.class);
        userService.update(user);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @PutMapping("/user")
    @ApiOperation(value = "Update user password")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateUser(@Valid @RequestBody UserUpdatePasswordDTO dto) {
        userService.updateUserPasswordByEmail(dto);
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("/users/{id}")
    @ApiOperation(value = "Delete a user")
    public void deleteUserById(@PathVariable Long id) {
        userService.deleteById(id);
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/users/{id}")
    @ApiOperation(value = "Search a user by ID", response = UserReadDTO.class)
    @ApiResponse(code = 404, message = "The user is ot found")
    public UserReadDTO getUserById(@PathVariable Long id) {
        User user = userService.getById(id);
        return mapper.simpleFieldMap(user, UserReadDTO.class);
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping("/users")
    @ApiOperation(value = "View a list of available users", response = UserReadDTO.class, responseContainer = "List")
    public List<UserReadDTO> getAllUsers() {
        List<User> users = userService.getAll();
        return mapper.listSimpleFieldMap(users, UserReadDTO.class);
    }
}
