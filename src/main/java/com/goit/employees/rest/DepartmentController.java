package com.goit.employees.rest;

import com.goit.employees.dto.DepartmentCreateDTO;
import com.goit.employees.dto.DepartmentUpdateDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Department;
import com.goit.employees.services.impl.DepartmentServiceImpl;
import com.goit.employees.swagger.dummy.department.DepartmentDummy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "departmentservice", description = "Operations with departments")
public class DepartmentController {

    @Autowired
    private PersistenceDtoMapper mapper;
    @Autowired
    private DepartmentServiceImpl departmentService;

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PostMapping("/departments")
    @ApiOperation(value = "Add a department")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addDepartment(@Valid @RequestBody DepartmentCreateDTO dto) {
        Department department = mapper.map(dto, Department.class);
        departmentService.add(department);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @PutMapping("/departments")
    @ApiOperation(value = "Update a department")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateDepartment(@Valid @RequestBody DepartmentUpdateDTO dto) {
        Department department = mapper.map(dto, Department.class);
        departmentService.update(department);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
    @DeleteMapping("/departments/{id}")
    @ApiOperation(value = "Delete a department")
    public void deleteDepartmentByID(@PathVariable Long id) {
        departmentService.deleteById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/departments/{id}")
    @ApiOperation(value = "Search a department by ID", response = DepartmentDummy.class)
    public Department getDepartment(@PathVariable Long id) {
        return departmentService.getById(id);
    }

    @Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
    @GetMapping("/departments")
    @ApiOperation(value = "View a list of available departments", response = DepartmentDummy.class, responseContainer = "List")
    public List<Department> getAllDepartment() {
        return departmentService.getAll();
    }

}
