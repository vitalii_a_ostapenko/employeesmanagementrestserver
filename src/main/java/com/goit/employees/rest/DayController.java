package com.goit.employees.rest;

import com.goit.employees.dto.DayCreateDTO;
import com.goit.employees.dto.DayUpdateDTO;
import com.goit.employees.mapper.PersistenceDtoMapper;
import com.goit.employees.model.Day;
import com.goit.employees.services.impl.DayServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
@RestController
@Api(value = "Day Service", description = "Operations with Days")
public class DayController {

    @Autowired
    private PersistenceDtoMapper mapper;

    @Autowired
    private DayServiceImpl dayService;

    @PostMapping("/days")
    @ApiOperation("Add Day")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void addDay(@Valid @RequestBody DayCreateDTO dto) {
        Day day = mapper.map(dto, Day.class);
        dayService.add(day);
    }

    @PutMapping("/days")
    @ApiOperation("Update Day")
    @ApiResponses(@ApiResponse(code = 400, message = "Wrong arguments"))
    public void updateDay(@Valid @RequestBody DayUpdateDTO dto) {
        Day day = mapper.map(dto, Day.class);
        dayService.update(day);
    }

    @DeleteMapping("/days/{id}")
    @ApiOperation("Delete Day by id")
    public void deleteDayById(@PathVariable Long id) {
        dayService.deleteById(id);
    }

    @GetMapping("/days/{id}")
    @ApiOperation("Get Day by id")
    public Day getDayById(@PathVariable Long id) {
        return dayService.getById(id);
    }

    @GetMapping("/days")
    @ApiOperation(value = "Get all Days", response = Day.class, responseContainer = "List")
    public List<Day> getAllDays() {
        return dayService.getAll();
    }
}
