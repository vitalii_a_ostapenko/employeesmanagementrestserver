package com.goit.employees.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ConfigurationProperties(prefix = "payment.coefficient")
@Data
@NoArgsConstructor
public class TimeTypeCoefficientProperties {
    private BigDecimal nightTime;
    private BigDecimal weekend;
    private BigDecimal overTime;
}
