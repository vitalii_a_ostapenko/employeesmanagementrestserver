package com.goit.employees.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ConfigurationProperties(prefix = "sick.coefficient")
@Data
@NoArgsConstructor
public class SickLeaveCoefficientProperties {
    private BigDecimal toThreeYear;
    private BigDecimal toFiveYear;
    private BigDecimal toEightYear;
    private BigDecimal fromEightYear;
}