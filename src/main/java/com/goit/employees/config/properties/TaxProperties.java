package com.goit.employees.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@ConfigurationProperties(prefix = "tax")
@Data
@NoArgsConstructor
public class TaxProperties {
    private BigDecimal personalIncomeTax;
    private BigDecimal militaryTax;
}

