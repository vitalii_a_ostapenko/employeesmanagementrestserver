package com.goit.employees.services;

import java.util.List;

public interface IService<T> {

    void add(T entity);

    void update(T entity);

    void deleteById(Long id);

    T getById(Long id);

    List<T> getAll();
}
