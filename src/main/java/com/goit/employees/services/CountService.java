package com.goit.employees.services;

import com.goit.employees.config.properties.SickLeaveCoefficientProperties;
import com.goit.employees.config.properties.TaxProperties;
import com.goit.employees.config.properties.TimeTypeCoefficientProperties;
import com.goit.employees.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * The class contains methods for count working hours, days of vacancy, illness, amount of
 * compensations for vacancy or illness, taxes, total salary and salary to payed.
 * This class is used as utility class
 */
@Service
public class CountService {
    /**
     * The field of taxes rates
     */
    @Autowired
    private TaxProperties taxProperties;

    /**
     * The field of coefficients for counting compensation for illness
     */
    @Autowired
    private SickLeaveCoefficientProperties sickLeaveCoefficientProperties;

    /**
     * The field of coefficients for counting the rate of overtime, nighttime working and working
     * in weekend or holidays
     */
    @Autowired
    private TimeTypeCoefficientProperties timeTypeCoefficientProperties;

    /**
     * The method count amount for count taxes
     *
     * @param events   - events for year
     * @param employee - employee
     * @param sheets   - settlement sheets for year
     * @param days     - calendar days
     * @param monthAgo - date a month ago
     * @return salary with compensations of vacancy and illness
     */
    private BigDecimal totalSalary(List<Event> events, Employee employee, List<SettlementSheet> sheets,
                                   List<Day> days, Date monthAgo) {
        return countSalary(employee, events, monthAgo).add(countSickLeave(employee, sheets, events, monthAgo))
                .add(countVacancy(sheets, events, days, monthAgo));
    }

    /**
     * The method counts a military tax
     *
     * @param events   - events for year
     * @param employee - employee
     * @param sheets   - settlement sheets for year
     * @param days     - calendar days for year
     * @param monthAgo - date a month ago
     * @return military tax
     */
    public BigDecimal militaryTax(List<Event> events, Employee employee,
                                  List<SettlementSheet> sheets, List<Day> days, Date monthAgo) {
        return totalSalary(events, employee, sheets, days, monthAgo)
                .multiply(taxProperties.getMilitaryTax());
    }

    /**
     * The method counts personal income tax
     *
     * @param events   - events for year
     * @param employee - employee
     * @param sheets   - settlement sheets for year
     * @param days     - calendar days for year
     * @param monthAgo - date a month ago
     * @return personal income tax
     */
    public BigDecimal personalIncomeTax(List<Event> events, Employee employee,
                                        List<SettlementSheet> sheets, List<Day> days, Date monthAgo) {
        return totalSalary(events, employee, sheets, days, monthAgo)
                .multiply(taxProperties.getPersonalIncomeTax());
    }

    /**
     * Method counts salary to pa without taxes
     *
     * @param events   - events for year
     * @param employee - employee
     * @param sheets   - settlement sheets for year
     * @param days     - calendar days for year
     * @param monthAgo - date a month ago
     * @return salary for payment
     */
    public BigDecimal salaryToPay(List<Event> events, Employee employee,
                                  List<SettlementSheet> sheets, List<Day> days, Date monthAgo) {
        return totalSalary(events, employee, sheets, days, monthAgo)
                .add(militaryTax(events, employee, sheets, days, monthAgo).negate())
                .add(personalIncomeTax(events, employee, sheets, days, monthAgo).negate());
    }

    /**
     * The method counts salary for month including nighttime, overtime, weekend and holidays
     *
     * @param employee - employee
     * @param events   - events for year
     * @param monthAgo - date a month ago
     * @return salary for month
     */
    private BigDecimal countSalary(Employee employee, List<Event> events, Date monthAgo) {
        return countSalaryWorkingHours(employee, events, monthAgo).add(countSalaryNightHours(employee, events, monthAgo))
                .add(countSalaryOvertimeHours(employee, events, monthAgo))
                .add(countSalaryWeekendHolidayHours(employee, events, monthAgo));
    }

    /**
     * The method counts a compensation of sick leave
     *
     * @param employee - employee
     * @param sheets   - settlement sheets for year
     * @param events   - events for year
     * @param monthAgo - date a month ago
     * @return compensation
     */
    public BigDecimal countSickLeave(Employee employee, List<SettlementSheet> sheets, List<Event> events,
                                     Date monthAgo) {
        BigDecimal coef = findSickCoefficient(employee);
        return BigDecimal.valueOf(dayCountSickLeave(events, monthAgo)).multiply(coef)
                .multiply(countAverageSalaryForSick(sheets, events, monthAgo));
    }

    /**
     * Method counts a compensation for vacancy
     *
     * @param sheets   - settlement sheets
     * @param events   - all events
     * @param days     - calendar days of year
     * @param monthAgo - date month ago
     * @return compensation
     */
    public BigDecimal countVacancy(List<SettlementSheet> sheets, List<Event> events, List<Day> days,
                                   Date monthAgo) {
        double daysForVacancy = countDaysInYearForVacancy(events, days, monthAgo);
        if (daysForVacancy != 0) {
            return countYearSalary(sheets, monthAgo)
                    .divide(BigDecimal.valueOf(daysForVacancy), 2, BigDecimal.ROUND_HALF_UP)
                    .multiply(BigDecimal.valueOf(countVacancyDays(events, monthAgo)));
        } else return BigDecimal.valueOf(0);
    }

    //************** Additional methods ************************************************************
//************** Methods for Sick leave ********************************************************

    /**
     * The method counts working months from employment date
     *
     * @param employee - employee
     * @return the number of working months
     */
    private int countWorkingMonth(Employee employee) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dateYear = calendar.get(Calendar.YEAR);
        int dateMonth = calendar.get(Calendar.MONTH);
        calendar.setTime(employee.getEmploymentDate());
        int employmentDateYear = calendar.get(Calendar.YEAR);
        int employmentMonth = calendar.get(Calendar.MONTH);
        return (dateYear - employmentDateYear) * 12 + (dateMonth - employmentMonth);
    }

    /**
     * The method counts amount of all salaries during the year
     *
     * @param sheets - settlement sheets
     * @param monthAgo  - date one month ago
     * @return amount of salaries
     */
    private BigDecimal countYearSalary(List<SettlementSheet> sheets, Date monthAgo) {

        BigDecimal sumYearSalary = BigDecimal.valueOf(0);
        if (sheets != null) {
            for (SettlementSheet sheet : sheets) {
                if (sheet.getDate().getTime() <= monthAgo.getTime()) {
                    sumYearSalary = sumYearSalary.add(sheet.getToPayoff()).add(sheet.getMilitaryTax())
                            .add(sheet.getPersonalIncomeTax()).add(sheet.getVacation());
                }
            }
        }
        return sumYearSalary;
    }

    /**
     * The method counts calendar days for year or working period if this one is less then one year
     *
     * @param events - all events
     * @param monthAgo - date one month ago
     * @return the number of days
     */
    private int countCalendarDays(List<Event> events, Date monthAgo) {
        int calendarDays = 0;
        if (events != null) {
            events.sort(Comparator.comparing(Event::getDate));
            int i = 0;
                for (Event event : events) {
                    if (event.getDate() != null && event.getDate().getTime() < monthAgo.getTime()) {
                        if (i == 0) {
                            calendarDays = (int) ((monthAgo.getTime() - event.getDate().getTime()) / (24 * 60 * 60 * 1000));
                        }
                        i++;
                    }
                }
        }
        return calendarDays;
    }

    /**
     * The method counts average annual salary for sick leave
     *
     * @param sheets - sheets of the previous periods
     * @param events - all events for previous period
     * @param monthAgo - date one month ago
     * @return average salary for year (or less)
     */
    private BigDecimal countAverageSalaryForSick(List<SettlementSheet> sheets,
                                                List<Event> events, Date monthAgo) {
        BigDecimal calendarDays = BigDecimal.valueOf(countCalendarDays(events, monthAgo) - dayAbsentInYearCount(events, monthAgo)
                - daySickLeaveInYearCount(events, monthAgo));
        if (calendarDays.doubleValue() > 0.99) {
            return countYearSalary(sheets, monthAgo).divide(calendarDays, 2, BigDecimal.ROUND_HALF_UP);
        } else return BigDecimal.valueOf(0);
    }

    /**
     * The method counts days of illness for month
     *
     * @param events   - all events for month
     * @param monthAgo - date one month ago
     * @return the number of days of illness
     */
    private int dayCountSickLeave(List<Event> events, Date monthAgo) {
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        int sumDaysSickLeaves = 0;
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventType() != null && event.getEventType().equals(EventType.HOSPITAL)) {
                    sumDaysSickLeaves++;
                }
            }
        }
        return sumDaysSickLeaves;
    }

    /**
     * The method takes an employee and chooses a coefficient depend on his experience term
     *
     * @param employee - employee
     * @return coefficient
     */
    private BigDecimal findSickCoefficient(Employee employee) {
        BigDecimal coef;
        double experience = countWorkingMonth(employee) / 12 + employee.getStartingWorkExperience();
        if (experience < 3) {
            coef = sickLeaveCoefficientProperties.getToThreeYear();
        } else {
            if (experience < 5) {
                coef = sickLeaveCoefficientProperties.getToFiveYear();
            } else {
                if (experience < 8) {
                    coef = sickLeaveCoefficientProperties.getToEightYear();
                } else coef = sickLeaveCoefficientProperties.getFromEightYear();
            }
        }
        return coef;
    }

    //************* Methods for Vacancy ************************************************************

    /**
     * The method counts calendar days in year without holidays and days of absence including sick leave
     *
     * @param events - events for a year
     * @param days   - calendar's days for year
     * @param monthAgo - date one month ago
     * @return tne number of days without holidays and non-working days
     */
    private int countDaysInYearForVacancy(List<Event> events, List<Day> days, Date monthAgo) {
        return countCalendarDays(events, monthAgo) - dayAbsentInYearCount(events, monthAgo)
                - dayHolidaysInYearCount(days, monthAgo) - daySickLeaveInYearCount(events, monthAgo);
    }

    /**
     * The method counts non-working days during a year
     *
     * @param events - all events for year
     * @param monthAgo - date one month ago
     * @return number of absent's days
     */
    private int dayAbsentInYearCount(List<Event> events, Date monthAgo) {
        int absentDays = 0;
        if (events != null) {
            for (Event event : events) {
                if (event.getEventType() != null && event.getDate().getTime() < monthAgo.getTime()) {
                    if (event.getEventType().equals(EventType.ABSENT)) {
                        absentDays++;
                    }
                }
            }
        }
        return absentDays;
    }

    /**
     * The method counts days of illness during a year
     *
     * @param events - all events for year
     * @param monthAgo - date one month ago
     * @return number of absent's days
     */
    private int daySickLeaveInYearCount(List<Event> events, Date monthAgo) {
        int sickDays = 0;
        if (events != null) {
            for (Event event : events) {
                if (event.getEventType() != null && event.getDate().getTime() < monthAgo.getTime()) {
                    if (event.getEventType().equals(EventType.HOSPITAL)) {
                        sickDays++;
                    }
                }
            }
        }
        return sickDays;
    }

    /**
     * The method counts the number of holidays during th year
     *
     * @param days - calendar days with a marker of holidays, working or weekend
     * @param monthAgo - date one month ago
     * @return the number of days
     */
    private int dayHolidaysInYearCount(List<Day> days, Date monthAgo) {
        int holidayDays = 0;
        if (days != null) {
            for (Day day : days) {
                if (day.getDayType() != null && day.getDate().getTime() < monthAgo.getTime()) {
                    if (day.getDayType().equals(DayType.HOLIDAY)) {
                        holidayDays++;
                    }
                }
            }
        }
        return holidayDays;
    }

    /**
     * The method chooses events from the pointed date to the end
     *
     * @param events - all events for year
     * @param toDate - date from which events are chosen
     * @return evens for period
     */
    private List<Event> countToDateEvent(List<Event> events, Date toDate) {
        List<Event> periodEvent = new ArrayList<>();
        if (events != null)
            for (Event event : events) {
                if (event.getDate() != null) {
                    if (event.getDate().getTime() >= toDate.getTime()) {
                        periodEvent.add(event);
                    }
                }
            }
        return periodEvent;
    }

    /**
     * The method counts the number of vacancy days for month
     *
     * @param events   - all events for month
     * @param monthAgo - date one month ago
     * @return the number of vacancy days
     */
    private int countVacancyDays(List<Event> events, Date monthAgo) {
        int vacancyDays = 0;
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventType() != null && event.getEventType().equals(EventType.VACATION)) {
                    vacancyDays++;
                }
            }
        }
        return vacancyDays;
    }

//*************** Working days *******************************************************************

    /**
     * The method counts night working salary for month
     *
     * @param employee - employee
     * @param events   - all events for month
     * @param monthAgo - date one month ago
     * @return salary of night working hours
     */
    private BigDecimal countSalaryNightHours(Employee employee, List<Event> events, Date monthAgo) {
        int nightHours = countNightHours(events, monthAgo);
        if (nightHours != 0) {
            return employee.getPosition().getHourlyRate().multiply(BigDecimal.valueOf(nightHours))
                    .multiply(timeTypeCoefficientProperties.getNightTime());
        } else return BigDecimal.valueOf(0);
    }

    /**
     * The mothod counts overtime salary for month
     *
     * @param employee - employee
     * @param events   - all events for month
     * @param monthAgo - date one month ago
     * @return salary of overtime hours
     */
    private BigDecimal countSalaryOvertimeHours(Employee employee, List<Event> events, Date monthAgo) {
        int overtimeHours = countOvertimeHours(events, monthAgo);
        if (overtimeHours != 0) {
            return employee.getPosition().getHourlyRate().multiply(BigDecimal.valueOf(overtimeHours))
                    .multiply(timeTypeCoefficientProperties.getOverTime());
        } else return BigDecimal.valueOf(0);
    }

    /**
     * The method counts working weekend and holidays salary
     *
     * @param employee - employee
     * @param events   - all events for one month
     * @param monthAgo - date one month ago
     * @return salary of working weekend's and holidays hours
     */
    private BigDecimal countSalaryWeekendHolidayHours(Employee employee, List<Event> events, Date monthAgo) {
        int weekendHours = countWeekendHolidayHours(events, monthAgo);
        if (weekendHours != 0) {
            return employee.getPosition().getHourlyRate().multiply(BigDecimal.valueOf(weekendHours))
                    .multiply(timeTypeCoefficientProperties.getWeekend());
        } else return BigDecimal.valueOf(0);
    }

    /**
     * The method counts a salary for working hours without night, overtime, weekend and holidays working hours
     * with using a normal hourly rate
     *
     * @param employee - the employee
     * @param events   - all events for one month
     * @param monthAgo - date one month ago
     * @return the salary of normal working hours, or minimal salary if the working hours were less then minimal salary
     */
    private BigDecimal countSalaryWorkingHours(Employee employee, List<Event> events, Date monthAgo) {
        BigDecimal salaryWorkingHours = employee.getPosition().getHourlyRate().multiply(BigDecimal.valueOf
                (countMonthWorkingHours(events, monthAgo)));

        BigDecimal salaryMin = (BigDecimal.valueOf(countMonthWorkingHours(events, monthAgo) * 19.34));
        if (salaryWorkingHours.doubleValue() <= 0 || salaryMin.doubleValue() <= 0) {
            return BigDecimal.valueOf(0);
        }
        if ((salaryMin.doubleValue() - salaryWorkingHours.doubleValue() > 0)) {
            return salaryMin;
        } else return salaryWorkingHours;
    }

    /**
     * The method counts working hours for one month
     *
     * @param events   - all events for one month
     * @param monthAgo - date one month ago
     * @return the number of hours
     */
    private int countMonthWorkingHours(List<Event> events, Date monthAgo) {
        int workingHours = 0;
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventTimeTypes() != null) {
                    for (EventTimeType eventTimeType : event.getEventTimeTypes()) {
                        if (eventTimeType.equals(EventTimeType.WORKDAY)) {
                            workingHours += event.getHours();
                        }
                    }
                }
            }
        }
        return workingHours;
    }

    /**
     * The method counts night working hours for one month
     *
     * @param events   - all events for one month
     * @param monthAgo - date one month ago
     * @return the number of hours
     */
    private int countNightHours(List<Event> events, Date monthAgo) {
        int nightHours = 0;
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventTimeTypes() != null) {
                    for (EventTimeType eventTimeType : event.getEventTimeTypes()) {
                        if (eventTimeType.equals(EventTimeType.NIGHT_TIME)) {
                            nightHours += event.getHours();
                        }
                    }
                }
            }
        }
        return nightHours;
    }

    /**
     * The method counts overtime working hours
     *
     * @param events   - all events for one month
     * @param monthAgo - date one month ago
     * @return the number of hours
     */
    private int countOvertimeHours(List<Event> events, Date monthAgo) {
        int overtimeHours = 0;
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventTimeTypes() != null) {
                    for (EventTimeType eventTimeType : event.getEventTimeTypes()) {
                        if (eventTimeType.equals(EventTimeType.OVER_TIME)) {
                            overtimeHours += event.getHours();
                        }
                    }
                }
            }
        }
        return overtimeHours;
    }

    /**
     * The method counts working hours in weekend and holidays
     *
     * @param events   - all events for one month
     * @param monthAgo - the date one month ago
     * @return the number of hours
     */
    private int countWeekendHolidayHours(List<Event> events, Date monthAgo) {
        int weekendHolidayHours = 0;
        List<Event> monthEvents = countToDateEvent(events, monthAgo);
        if (monthEvents != null) {
            for (Event event : monthEvents) {
                if (event.getEventTimeTypes() != null) {
                    for (EventTimeType eventTimeType : event.getEventTimeTypes()) {
                        if (eventTimeType.equals(EventTimeType.HOLIDAY) ||
                                eventTimeType.equals(EventTimeType.WEEKEND)) {
                            weekendHolidayHours += event.getHours();
                        }
                    }
                }
            }
        }
        return weekendHolidayHours;
    }
}
