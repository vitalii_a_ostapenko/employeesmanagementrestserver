package com.goit.employees.services.impl;

import com.goit.employees.model.Employee;
import com.goit.employees.repositories.EmployeeRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The service's layer of application for employee, contains CRUD methods
 */
@Service
public class EmployeeServiceImpl implements IService<Employee> {

    /**
     * The field of employee repository's layer that is called for use it's methods
     */
    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * The method calls a repository's method for save an employee
     *
     * @param employee - employee
     */
    @Override
    public void add(Employee employee) {
        employeeRepository.save(employee);
    }

    /**
     * The method calls a repository's method for save an employee
     *
     * @param employee - employee
     */
    @Override
    public void update(Employee employee) {
        employeeRepository.save(employee);
    }

    /**
     * The method calls repository's method for delete an employee by ID
     *
     * @param id - employee's ID
     */
    @Override
    public void deleteById(Long id) {
        employeeRepository.delete(id);
    }

    /**
     * The method calls repository's method for find an employee by ID
     *
     * @param id - employee's ID
     * @return employee
     */
    @Override
    public Employee getById(Long id) {
        return employeeRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all employees
     *
     * @return all employees
     */
    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }
}
