package com.goit.employees.services.impl;

import com.goit.employees.services.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * The service's layer of application for email, contains send simple message method for a
 * {@link com.goit.employees.services.SettlementSheetPersistentCountService} class
 */
@Component
@Slf4j
public class EmailServiceImpl implements EmailService {

    /**
     * The field of {@link JavaMailSender} layer that is called for use it's methods
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * The method compose and send a simple email message
     *
     * @param to - indicates the e-mail address of the recipient
     * @param subject - subject message
     * @param text - text message
     */
    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            mailSender.send(message);
            log.info("Email to {} sent successfully", message.getTo()[0]);
        } catch (MailException e) {
            log.error("Unable to send an email message!", e);
        }
    }
}
