package com.goit.employees.services.impl;

import com.goit.employees.model.Day;
import com.goit.employees.repositories.DayRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The service's layer of application for day, contains CRUD methods
 */
@Service
public class DayServiceImpl implements IService<Day> {

    /**
     * The field of day repository's layer that is called for use it's methods
     */
    @Autowired
    private DayRepository dayRepository;

    /**
     * The method calls a repository's method for save a day
     *
     * @param day - day
     */
    @Override
    public void add(Day day) {
        dayRepository.save(day);
    }

    /**
     * The method calls a repository's method for save a day
     *
     * @param day - day
     */
    @Override
    public void update(Day day) {
        dayRepository.save(day);
    }

    /**
     * The method calls repository's method for delete a day by ID
     *
     * @param id - ID of the day for delete
     */
    @Override
    public void deleteById(Long id) {
        dayRepository.delete(id);
    }

    /**
     * The method calls repository's method for find a day by ID
     *
     * @param id - day's ID
     * @return day
     */
    @Override
    public Day getById(Long id) {
        return dayRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all days
     *
     * @return all days
     */
    @Override
    public List<Day> getAll() {
        return dayRepository.findAll();
    }
}
