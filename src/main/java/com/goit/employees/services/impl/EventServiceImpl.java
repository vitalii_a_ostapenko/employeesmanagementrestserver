package com.goit.employees.services.impl;

import com.goit.employees.exception.EmployeeNotAbleToWorkException;
import com.goit.employees.model.Employee;
import com.goit.employees.model.EmployeeStatus;
import com.goit.employees.model.Event;
import com.goit.employees.model.EventType;
import com.goit.employees.repositories.EventRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The service's layer of application for event, contains CRUD methods
 */
@Service
public class EventServiceImpl implements IService<Event> {

    /**
     * The field of event repository's layer that is called for use it's methods
     */
    @Autowired
    private EventRepository eventRepository;

    /**
     * The method calls a repository's method for save an event
     *
     * @param event - event
     */
    @Override
    public void add(Event event) {
        abilityToWorkCheck(event);
        eventRepository.save(event);
    }

    /**
     * The method controls a correct status of employee, and throws handler exception
     * {@link EmployeeNotAbleToWorkException}
     * @param event - event that should be checked
     */
    private void abilityToWorkCheck(Event event) {
        if (event.getEventType() == EventType.WORKDAY || event.getEventType() == EventType.TECHNICAL_TRAINING
                || event.getEventType() == EventType.TECHNICAL_TRAINING || event.getEventType() == EventType.ABSENT) {
            for (Employee employee : event.getEmployees()) {
                EmployeeStatus status = employee.getStatus();
                if (status != EmployeeStatus.ABLE_TO_WORK) {
                    throw new EmployeeNotAbleToWorkException
                            (String.format("Employee id: %d status: %s, he can't work", employee.getId(), status));
                }
            }
        }
    }

    /**
     * The method calls a repository's method for save an event
     *
     * @param event - event
     */
    @Override
    public void update(Event event) {
        abilityToWorkCheck(event);
        eventRepository.save(event);
    }

    /**
     * The method calls repository's method for delete an event by ID
     *
     * @param id - event's ID
     */
    @Override
    public void deleteById(Long id) {
        eventRepository.delete(id);
    }

    /**
     * The method calls repository's method for find an event by ID
     *
     * @param id - event's ID
     * @return event
     */
    @Override
    public Event getById(Long id) {
        return eventRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all events
     *
     * @return all events
     */
    @Override
    public List<Event> getAll() {
        return eventRepository.findAll();
    }
}
