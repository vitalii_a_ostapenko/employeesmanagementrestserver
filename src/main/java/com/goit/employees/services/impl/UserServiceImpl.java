package com.goit.employees.services.impl;

import com.goit.employees.dto.UserUpdatePasswordDTO;
import com.goit.employees.model.User;
import com.goit.employees.repositories.UserRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The service's layer of application for user, contains CRUD methods
 */
@Service
public class UserServiceImpl implements IService<User> {

    @Autowired
    private PasswordEncoder encoder;

    /**
     * The field of user repository's layer that is called for use it's methods
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * The method calls a repository's method for save a user
     *
     * @param user - user
     */
    @Override
    public void add(User user) {
        encodePassword(user);
        userRepository.save(user);
    }

    /**
     * The method calls a repository's method for save a user
     *
     * @param user - user
     */
    @Override
    public void update(User user) {
        encodePassword(user);
        userRepository.save(user);
    }

    @Transactional
    public void updateUserPasswordByEmail(UserUpdatePasswordDTO dto) {
        User user = userRepository.findByEmail(dto.getEmail());
        if (user == null) {
            throw  new EntityNotFoundException(String.format("There is no User with email: %s", dto.getEmail()));
        }
        if (encoder.matches(dto.getOldPassword(), user.getPassword())) {
            user.setPassword(encoder.encode(dto.getPassword()));
        } else {
            throw new BadCredentialsException("Old password is wrong");
        }
    }

    /**
     * The method calls repository's method for delete a user by ID
     *
     * @param id - user's ID
     */
    @Override
    public void deleteById(Long id) {
        userRepository.delete(id);
    }

    /**
     * The method calls repository's method for find a user by ID
     *
     * @param id - user's ID
     * @return user
     */
    @Override
    public User getById(Long id) {
        return userRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all users
     *
     * @return all users
     */
    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    private void encodePassword(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
    }
}
