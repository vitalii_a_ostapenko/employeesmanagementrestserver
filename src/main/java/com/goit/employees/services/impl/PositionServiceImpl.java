package com.goit.employees.services.impl;

import com.goit.employees.model.Position;
import com.goit.employees.repositories.PositionRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The service's layer of application for position, contains CRUD methods
 */
@Service
public class PositionServiceImpl implements IService<Position> {

    /**
     * The field of position repository's layer that is called for use it's methods
     */
    @Autowired
    private PositionRepository positionRepository;

    /**
     * The method calls a repository's method for save a position
     *
     * @param position - position
     */
    @Override
    public void add(Position position) {
        positionRepository.save(position);
    }

    /**
     * The method calls a repository's method for save a position
     *
     * @param position - position
     */
    @Override
    public void update(Position position) {
        positionRepository.save(position);
    }

    /**
     * The method calls repository's method for delete a position by ID
     *
     * @param id - position's ID
     */
    @Override
    public void deleteById(Long id) {
        positionRepository.delete(id);
    }

    /**
     * The method calls repository's method for find a position by ID
     *
     * @param id - position's ID
     * @return position
     */
    @Override
    public Position getById(Long id) {
        return positionRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all positions
     *
     * @return all positions
     */
    @Override
    public List<Position> getAll() {
        return positionRepository.findAll();
    }
}
