package com.goit.employees.services.impl;

import com.goit.employees.model.Department;
import com.goit.employees.repositories.DepartmentRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The service's layer of application for department, contains CRUD methods
 */
@Service
public class DepartmentServiceImpl implements IService<Department> {

    /**
     * The field of department repository's layer that is called for use it's methods
     */
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * The method calls a repository's method for save a department
     *
     * @param department - department
     */
    @Override
    public void add(Department department) {
        departmentRepository.save(department);
    }

    /**
     * The method calls a repository's method for save a department
     *
     * @param department - department
     */
    @Override
    public void update(Department department) {
        departmentRepository.save(department);
    }

    /**
     * The method calls repository's method for delete a department by ID
     *
     * @param id - department's ID
     */
    @Override
    public void deleteById(Long id) {
        departmentRepository.delete(id);
    }

    /**
     * The method calls repository's method for find a department by ID
     *
     * @param id - department's ID
     * @return department
     */
    @Override
    public Department getById(Long id) {
        return departmentRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all departments
     *
     * @return all departments
     */
    @Override
    public List<Department> getAll() {
        return departmentRepository.findAll();
    }
}
