package com.goit.employees.services.impl;

import com.goit.employees.model.Employee;
import com.goit.employees.model.SettlementSheet;
import com.goit.employees.repositories.SettlementSheetRepository;
import com.goit.employees.services.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * The service's layer of application for settlement sheet, contains CRUD methods
 */
@Service
public class SettlementSheetServiceImpl implements IService<SettlementSheet> {

    /**
     * The field of settlement sheet repository's layer that is called for use it's methods
     */
    @Autowired
    private SettlementSheetRepository settlementSheetRepository;

    /**
     * The method calls a repository's method for save a settlement sheet
     *
     * @param settlementSheet - settlement sheet
     */
    @Override
    public void add(SettlementSheet settlementSheet) {
        settlementSheetRepository.save(settlementSheet);
    }

    /**
     * The method calls a repository's method for save a settlement sheet
     *
     * @param settlementSheet - settlement sheet
     */
    @Override
    public void update(SettlementSheet settlementSheet) {
        settlementSheetRepository.save(settlementSheet);
    }

    /**
     * The method calls repository's method for delete a settlement sheet by ID
     *
     * @param id - settlement sheet's ID
     */
    @Override
    public void deleteById(Long id) {
        settlementSheetRepository.delete(id);
    }

    /**
     * The method calls repository's method for find a settlement sheet by ID
     *
     * @param id - settlement sheet's ID
     * @return settlement sheet
     */
    @Override
    public SettlementSheet getById(Long id) {
        return settlementSheetRepository.findOne(id);
    }

    /**
     * The method calls repository's method for find all settlement sheets
     *
     * @return all settlement sheets
     */
    @Override
    public List<SettlementSheet> getAll() {
        return settlementSheetRepository.findAll();
    }

    /**
     * The method calls repository's method for find settlement sheets of pointed employee for a period
     *
     * @param employee - employee
     * @param begin    - date of the beginning of a period
     * @param end      - date of end of a period
     * @return settlement sheets
     */
    public List<SettlementSheet> getByEmployeeAndDate(Employee employee, Date begin, Date end) {
        return settlementSheetRepository.findSettlementSheetsByEmployeeAndDateBetween(employee, begin, end);
    }
}
