package com.goit.employees.services;

import com.goit.employees.model.Day;
import com.goit.employees.model.Employee;
import com.goit.employees.model.Event;
import com.goit.employees.model.SettlementSheet;
import com.goit.employees.repositories.DayRepository;
import com.goit.employees.repositories.EventRepository;
import com.goit.employees.services.impl.EmailServiceImpl;
import com.goit.employees.services.impl.EmployeeServiceImpl;
import com.goit.employees.services.impl.SettlementSheetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Service's class for create a settlement sheet every 1-st date of month and send the information
 * to the employees
 */
@Service
public class SettlementSheetPersistentCountService {

    @Autowired
    private CountService countService;

    @Autowired
    private EmployeeServiceImpl employeeService;

    @Autowired
    private SettlementSheetServiceImpl settlementSheet;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private DayRepository dayRepository;

    @Autowired
    private EmailServiceImpl emailService;

    /**
     * Current date
     */
    private Date date = new Date();
    /**
     * Date one month ago from the current date
     */
    private Date monthAgo;
    /**
     * Date one year ago from the current date
     */
    private Date yearAgo;
    /**
     * Month of the settlement sheet, previous month of the current date
     */
    private Month currentMonth;
    Calendar calendar = Calendar.getInstance();

    /**
     * The method creates settlement sheets and send its by e-mail to every employee
     */
    @Scheduled(cron = "0 1 0 1 * ?")
    public void createSettlementSheets() {
        List<Employee> employees = employeeService.getAll();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        monthAgo = calendar.getTime();
        currentMonth = Month.of(calendar.get(Calendar.MONTH));
        calendar.add(Calendar.YEAR, -1);
        yearAgo = calendar.getTime();


        for (Employee employee : employees) {
            if (employee.getEmploymentDate().getTime() > monthAgo.getTime()) {
                monthAgo = employee.getEmploymentDate();
            }
            if (employee.getEmploymentDate().getTime() > yearAgo.getTime()) {
                yearAgo = employee.getEmploymentDate();
            }
            List<Day> days = dayRepository.findDaysByDateBetween(date, yearAgo);
            List<SettlementSheet> sheets = settlementSheet.getByEmployeeAndDate(employee, date, yearAgo);
            List<Event> events = eventRepository.findByEmployeesContainingAndDateBetween(employee, yearAgo, date);

            SettlementSheet sheet = new SettlementSheet();
            sheet.setDate(date);
            sheet.setEmployee(employee);
            sheet.setMilitaryTax(countService.militaryTax(events, employee, sheets,
                    days, monthAgo));
            sheet.setPersonalIncomeTax(countService.personalIncomeTax(events, employee,
                    sheets, days, monthAgo));
            sheet.setMonth(currentMonth);
            sheet.setSickLeave(countService.countSickLeave(employee, sheets, events, monthAgo));
            sheet.setVacation(countService.countVacancy(sheets, events, days, monthAgo));
            sheet.setToPayoff(countService.salaryToPay(events, employee, sheets,
                    days, monthAgo));

            settlementSheet.add(sheet);

            emailService.sendSimpleMessage(sheet.getEmployee().getUser().getEmail(), writeSheet(sheet), headText(sheet));
        }
    }

    /**
     * The method writes an information from settlement sheet about salary in reading-able form
     *
     * @param sheet settlement sheet
     * @return text
     */
    private String writeSheet(SettlementSheet sheet) {
        String textSheet = "\n   Settlement Sheet for " + sheet.getMonth() +
                "\n_________________________________________________" +
                "\n Date: " + sheet.getDate() +
                "\n Employee: " + sheet.getEmployee().getFirstName() + " " + sheet.getEmployee().getLastName() +
                "\n________________________________________________" +
                "\n\n\nTOTAL SALARY: " + (sheet.getPersonalIncomeTax().add(sheet.getMilitaryTax())
                .add(sheet.getToPayoff()).add(sheet.getSickLeave()).add(sheet.getVacation())) +
                "\nincluding: " +
                "\nVacancy:                    " + sheet.getVacation() +
                "\nCompensation of sick leave: " + sheet.getSickLeave() +
                "\n________________________________________________" +
                "\nPersonal income tax:        " + sheet.getPersonalIncomeTax() +
                "\nMilitary tax:               " + sheet.getMilitaryTax() +
                "\n________________________________________________" +
                "\nSALARY TO PAY:              " + sheet.getToPayoff();

        return textSheet;
    }

    /**
     * The method writes a head to the e-mail
     *
     * @param sheet - settlement sheet
     * @return text
     */
    private String headText(SettlementSheet sheet) {
        String textHead = "Salary Information Sheet for " + sheet.getMonth();
        return textHead;
    }
}
