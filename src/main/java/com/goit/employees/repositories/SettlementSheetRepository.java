package com.goit.employees.repositories;

import com.goit.employees.model.Employee;
import com.goit.employees.model.SettlementSheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SettlementSheetRepository extends JpaRepository<SettlementSheet, Long> {
    List<SettlementSheet> findSettlementSheetsByEmployeeAndDateBetween(Employee employee, Date begin, Date end);
}
