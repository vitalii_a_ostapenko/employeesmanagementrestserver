package com.goit.employees.repositories;

import com.goit.employees.model.Day;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DayRepository extends JpaRepository<Day, Long> {
    List<Day> findDaysByDateBetween(Date begin, Date end);
}
