package com.goit.employees.repositories;

import com.goit.employees.model.Employee;
import com.goit.employees.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findByEmployeesContainingAndDateBetween(Employee employee, Date begin, Date end);
}
