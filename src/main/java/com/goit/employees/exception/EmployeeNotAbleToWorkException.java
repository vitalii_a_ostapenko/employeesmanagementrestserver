package com.goit.employees.exception;

/**
 * Handler exception for method in {@link com.goit.employees.services.impl.EventServiceImpl}
 */
public class EmployeeNotAbleToWorkException extends RuntimeException {
    public EmployeeNotAbleToWorkException() {
    }

    public EmployeeNotAbleToWorkException(String message) {
        super(message);
    }
}
