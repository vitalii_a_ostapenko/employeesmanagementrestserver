package com.goit.employees.exception.handler;

import com.goit.employees.exception.EmployeeNotAbleToWorkException;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;

/**
 * The class contains total handler exceptions for all rest-layer.
 */
@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class RestResponseEntityExceptionHandler {

    /**
     * The method transforms a standart {@link DataIntegrityViolationException} to the mode of {@link ErrorBody}
     * @param ex - gotten exception
     * @param request - HTTP request
     * @return response entity
     */
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    protected ResponseEntity<ErrorBody> handleBadRequest(DataIntegrityViolationException ex, HttpServletRequest request) {
        log.debug("Exception handled successfully {}, path={}, msg={}",
                ex.getClass().getSimpleName(), request.getRequestURI(), getRootCauseMessage(ex));
        ErrorBody errorBody = getErrorBody(ex, request, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorBody, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * The method transforms a handle exception {@link EmployeeNotAbleToWorkException} to the mode of {@link ErrorBody}
     * @param ex - gotten exception
     * @param request - HTTP request
     * @return response entity
     */
    @ExceptionHandler(value = {EmployeeNotAbleToWorkException.class})
    protected ResponseEntity<ErrorBody> handleBadRequest(EmployeeNotAbleToWorkException ex, HttpServletRequest request) {
        log.debug("Exception handled successfully {}, path={}, msg={}",
                ex.getClass().getSimpleName(), request.getRequestURI(), getRootCauseMessage(ex));
        ErrorBody errorBody = getErrorBody(ex, request, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorBody, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * The method transforms an exception {@link EntityNotFoundException} to the mode of {@link ErrorBody}
     * @param ex - gotten exception
     * @param request - HTTP request
     * @return response entity
     */
    @ExceptionHandler(value = {EntityNotFoundException.class})
    protected ResponseEntity<ErrorBody> handleNotFound(EntityNotFoundException ex, HttpServletRequest request) {
        log.debug("Exception handled successfully {}, path={}, msg={}",
                ex.getClass().getSimpleName(), request.getRequestURI(), getRootCauseMessage(ex));
        ErrorBody errorBody = getErrorBody(ex, request, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorBody, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * The method transforms an exception {@link EmptyResultDataAccessException} to the mode of {@link ErrorBody}
     * @param ex - gotten exception
     * @param request - HTTP request
     * @return response entity
     */
    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    protected ResponseEntity<ErrorBody> handleNotFound(EmptyResultDataAccessException ex, HttpServletRequest request) {
        log.debug("Exception handled successfully {}, path={}, msg={}",
                ex.getClass().getSimpleName(), request.getRequestURI(), getRootCauseMessage(ex));
        ErrorBody errorBody = getErrorBody(ex, request, HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorBody, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    /**
     * The method transforms an exception {@link MethodArgumentNotValidException} to the mode of {@link ErrorBody}
     * @param ex - gotten exception
     * @param request - HTTP request
     * @return response entity
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    protected ResponseEntity<ErrorBody> handleBadRequest(MethodArgumentNotValidException ex, HttpServletRequest request) {
        log.debug("Exception handled successfully {}, path={}, msg={}",
                ex.getClass().getSimpleName(), request.getRequestURI(),
                ex.getBindingResult().getFieldErrors().get(0).getDefaultMessage());
        ErrorBody errorBody = getErrorBody(ex, request, HttpStatus.BAD_REQUEST);
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        errorBody.setMessage(fieldErrors.get(0).getDefaultMessage());
        return new ResponseEntity<>(errorBody, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * The method for transform a message of exception to the mode of {@link ErrorBody}, it gets parameters and
     * give to the new mode
     * @param ex - gotten exception
     * @param request - HTTP request
     * @param STATUS - HTTP status
     * @return message in a new mode
     */
    @NotNull
    private ErrorBody getErrorBody(Exception ex, HttpServletRequest request, HttpStatus STATUS) {
        ErrorBody errorBody = new ErrorBody();
        errorBody.setTimestamp(System.nanoTime());
        errorBody.setError(STATUS.getReasonPhrase());
        errorBody.setStatus(STATUS.value());
        errorBody.setException(ex.getClass().getName());
        errorBody.setMessage(getRootCauseMessage(ex));
        errorBody.setPath(request.getRequestURI());
        return errorBody;
    }
}