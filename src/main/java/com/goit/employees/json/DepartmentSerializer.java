package com.goit.employees.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.goit.employees.model.Department;
import com.goit.employees.model.Employee;
import com.goit.employees.model.Position;

import java.io.IOException;


public class DepartmentSerializer extends StdSerializer<Department> {

    public DepartmentSerializer() {
        super(Department.class);
    }

    protected DepartmentSerializer(Class<Department> t) {
        super(t);
    }

    protected DepartmentSerializer(JavaType type) {
        super(type);
    }

    protected DepartmentSerializer(Class<?> t, boolean dummy) {
        super(t, dummy);
    }

    protected DepartmentSerializer(StdSerializer<?> src) {
        super(src);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void serialize(Department value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("name", value.getName());
        gen.writeArrayFieldStart("positions");
        for (Position position : value.getPositions()) {
            if (position == null) {
                gen.writeNull();
                break;
            }
            gen.writeStartObject();
            gen.writeNumberField("id", position.getId());
            gen.writeStringField("name", position.getName());
            gen.writeNumberField("hourlyRate", position.getHourlyRate());
            Employee employee = position.getEmployee();
            if (employee != null) {
                gen.writeObjectFieldStart("employee");
                gen.writeNumberField("id", employee.getId());
                gen.writeStringField("firstName", employee.getFirstName());
                gen.writeStringField("lastName", employee.getLastName());
                gen.writeStringField("status", employee.getStatus().name());
                gen.writeEndObject();
            } else {
                gen.writeNullField("employee");
            }
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }
}
