package com.goit.employees.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.goit.employees.model.Department;
import com.goit.employees.model.Employee;
import com.goit.employees.model.Position;

import java.io.IOException;

public class EmployeeSerializer extends StdSerializer<Employee> {

    public EmployeeSerializer() {
        super(Employee.class);
    }

    protected EmployeeSerializer(Class<Employee> t) {
        super(t);
    }

    protected EmployeeSerializer(JavaType type) {
        super(type);
    }

    protected EmployeeSerializer(Class<?> t, boolean dummy) {
        super(t, dummy);
    }

    protected EmployeeSerializer(StdSerializer<?> src) {
        super(src);
    }

    @Override
    public void serialize(Employee value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("firstName", value.getFirstName());
        gen.writeStringField("lastName", value.getLastName());
        gen.writeStringField("status", value.getStatus().name());
        Position position = value.getPosition();
        gen.writeObjectFieldStart("position");
        gen.writeNumberField("id", position.getId());
        gen.writeStringField("name", position.getName());
        gen.writeNumberField("hourlyRate", position.getHourlyRate());
        Department department = position.getDepartment();
        gen.writeObjectFieldStart("department");
        gen.writeNumberField("id", department.getId());
        gen.writeStringField("name", department.getName());
        gen.writeEndObject();
        gen.writeEndObject();
        gen.writeEndObject();
    }
}
