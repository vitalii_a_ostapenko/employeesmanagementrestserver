package com.goit.employees.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.goit.employees.model.Department;
import com.goit.employees.model.Employee;
import com.goit.employees.model.Position;

import java.io.IOException;

public class PositionSerializer extends StdSerializer<Position> {

    public PositionSerializer() {
        super(Position.class);
    }

    protected PositionSerializer(Class<Position> t) {
        super(t);
    }

    protected PositionSerializer(JavaType type) {
        super(type);
    }

    protected PositionSerializer(Class<?> t, boolean dummy) {
        super(t, dummy);
    }

    protected PositionSerializer(StdSerializer<?> src) {
        super(src);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void serialize(Position value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("name", value.getName());
        gen.writeNumberField("hourlyRate", value.getHourlyRate());
        Department department = value.getDepartment();
        gen.writeObjectFieldStart("department");
        gen.writeNumberField("id", department.getId());
        gen.writeStringField("name", department.getName());
        gen.writeEndObject();
        Employee employee = value.getEmployee();
        if (employee != null) {
            gen.writeObjectFieldStart("employee");
            gen.writeNumberField("id", employee.getId());
            gen.writeStringField("firstName", employee.getFirstName());
            gen.writeStringField("lastName", employee.getLastName());
            gen.writeStringField("status", employee.getStatus().name());
            gen.writeEndObject();
        } else {
            gen.writeNullField("employee");
        }
        gen.writeEndObject();
    }
}
