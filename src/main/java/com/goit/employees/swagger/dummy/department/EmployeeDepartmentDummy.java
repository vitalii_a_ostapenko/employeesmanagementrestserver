package com.goit.employees.swagger.dummy.department;

import com.goit.employees.model.EmployeeStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDepartmentDummy {
    @ApiModelProperty(position = 1)
    private Long id;
    @ApiModelProperty(position = 2)
    private String firstName;
    @ApiModelProperty(position = 3)
    private String lastName;
    @ApiModelProperty(position = 4)
    private EmployeeStatus status;
}
