package com.goit.employees.swagger.dummy.position;

import com.goit.employees.swagger.dummy.department.EmployeeDepartmentDummy;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PositionDummy {
    @ApiModelProperty(position = 1)
    private Long id;
    @ApiModelProperty(position = 2)
    private BigDecimal hourlyRate;
    @ApiModelProperty(position = 3)
    private DepartmentPositionDummy department;
    @ApiModelProperty(position = 4)
    private String name;
    @ApiModelProperty(position = 5)
    private EmployeeDepartmentDummy employee;
}
