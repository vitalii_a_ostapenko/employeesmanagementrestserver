package com.goit.employees.swagger.dummy.employee;

import com.goit.employees.model.EmployeeStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeDummy {
    @ApiModelProperty(position = 1)
    private Long id;
    @ApiModelProperty(position = 2)
    private String firstName;
    @ApiModelProperty(position = 3)
    private String lastName;
    @ApiModelProperty(position = 4)
    private EmployeeStatus status;
    @ApiModelProperty(position = 5)
    private PositionEmployeeDummy position;
}
