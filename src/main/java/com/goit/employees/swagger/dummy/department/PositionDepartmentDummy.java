package com.goit.employees.swagger.dummy.department;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PositionDepartmentDummy {
    @ApiModelProperty(position = 1)
    private Long id;
    @ApiModelProperty(position = 2)
    private String name;
    @ApiModelProperty(position = 3)
    private BigDecimal hourlyRate;
    @ApiModelProperty(position = 4)
    private EmployeeDepartmentDummy employee;
}
